import tensorflow as tf
import functools


def pointwise_distortion(dataset):
    return dataset.map(pointwise_distortion_impl, num_parallel_calls=16)


def resize(dataset, size):
    return dataset.map(functools.partial(resize_impl, size), num_parallel_calls=16)


def pointwise_distortion_impl(input_image, ground_truth, metadata):
    """
    A python function to randomly apply different distortion techniques on the examples
    :param input_image:
    :param ground_truth:
    :param metadata:  a dict for metadata
    :return:
    """
    image = tf.cast(input_image, tf.float32)
    image = tf.div(image, tf.constant(255.0))

    image = tf.pow(image, tf.random_uniform([1], 0.5, 2.0)) # Random gamma
    image = tf.image.random_brightness(image, max_delta=32. / 255.)
    image = tf.image.random_contrast(image, lower=0.5, upper=1.5)
    image = tf.image.random_saturation(image, lower=0.5, upper=1.5)
    image = tf.image.random_hue(image, max_delta=0.1)
    image = tf.image.random_contrast(image, lower=0.5, upper=1.5)

    image = tf.clip_by_value(image, 0.0, 1.0)

    image = tf.multiply(image, tf.constant(255.0))
    input_image = tf.cast(image, tf.uint8)

    return input_image, ground_truth, metadata


def resize_impl(size, input_image, ground_truth, metadata):
    """
    A python function to resize examples and labels
    :param size: a tupple in (h, w) format
    :param input_image:
    :param ground_truth:
    :param metadata: a dict for metadata
    :return:
    """
    image = tf.expand_dims(input_image, axis=0)
    image = tf.image.resize_bilinear(image, size)
    image = tf.squeeze(image, axis=0)

    if ground_truth is not None :
        gt = tf.expand_dims(ground_truth, axis=0)
        gt = tf.image.resize_nearest_neighbor(gt, size)
        ground_truth = tf.squeeze(gt, axis=0)

    return image, ground_truth, metadata
