import os
import random
import tensorflow as tf
import numpy as np
from PIL import Image
from io import BytesIO


class CityscapesDatasetGenerator(object):
    """
    A class to generate custom tf.data.Dataset objects for training and validation purposes
    """

    def __init__(self, config):
        """
        :param config: A convenience config dict 
        """
        self.config = config
        self.paths = {}
        self.images_dir = config['images_in_dir']
        self.store_file = config["store_file_location"]
        self.ground_truth_folder = config["ground_truth_dir"]
        self.city_subset = config['city_subset']
        self.train_sequences = config["train_sequences_file"]
        self.test_sequences = config["test_sequences_file"]
        self.val_sequences = config["val_sequences_file"]

    def get_dataset(self, mode, include_groundtruth=True, include_filenames=False, many_to_many=False, time_steps=3,
                    input_size=(512, 1024), shuffle=False, batch_size=1):
        """
        The method to generate the td.data.Dataset
        
        :param mode: One of ["train", "val", "test"]
        :param include_groundtruth: If ground truth labels should be returned
        :param include_filenames: If the file names of the examples should be returned
        :param many_to_many: If ground truth should be returned for all examples or only the last one
        :param time_steps: The number of time steps for each example
        :param input_size: The dimensions of the input
        :param shuffle: If the dataset should be shuffled
        :param batch_size: The batch size of the constructed tupples
        :return: 
        """
        assert mode in ["train", "val", "test"], "You must specify a mode from train, val or test"

        examples = []

        def _read_input_file(key):
            """
            A python function that finds and decodes a training image, transforming it to tf.Tensor.
            To be executed by tensorflow during runtime.
            :param key: The filename passed as a binary tensor from internal tensorflow function
            :return: A tf.Tensor containing the image
            """
            imgFolder = os.path.join(self.images_dir, mode)

            def _get_image(key):
                """
                A python function which loads the image binary as string. To be executed by tensorflow during runtime
                :param key: The filename passed as a binary tensor from internal tensorflow function
                :return: A np array containing the image as string
                """
                file_name = key.decode('utf-8')
                city_name = file_name.split('_')[0]
                with open("{}/{}/{}".format(imgFolder, city_name, file_name), 'rb') as f:
                    image_str = f.read()
                return np.array(image_str).tostring()

            input_image = tf.image.decode_png(tf.py_func(_get_image, [key], tf.string, stateful=False), channels=3)
            image = tf.expand_dims(input_image, axis=0)
            image = tf.image.resize_bilinear(image, input_size) #correct
            image = tf.squeeze(image, axis=0)
            return image

        def _read_gt_file(key):
            """
            A python function that finds and decodes a validation image, transforming it to tf.Tensor.
            To be executed by tensorflow during runtime.
            :param key: The filename passed as a binary tensor from internal tensorflow function
            :return: A tf.Tensor containing the image
            """
            gtFolder = os.path.join(self.ground_truth_folder, mode)

            def _get_label(key):
                """
                A python function which loads the image binary as string. To be executed by tensorflow during runtime
                :param key: The filename passed as a binary tensor from internal tensorflow function
                :return: A np array containing the image as string
                """
                file_name = key.decode('utf-8')
                city_name = file_name.split('_')[0]
                gt_location = "{}/{}/{}".format(gtFolder, city_name, file_name)
                if os.path.exists(gt_location):
                    with open(gt_location, 'rb') as f:
                        image_str = f.read()
                    return np.array(image_str).tostring()
                else:
                    image_array = np.ones((input_size[0], input_size[1])) * 255
                    masked_image = Image.fromarray(image_array.astype(np.uint8))
                    buf = BytesIO()
                    masked_image.save(buf, format='PNG')
                    png = buf.getvalue()
                    return np.asarray(png).tostring()

            decoded_image = tf.py_func(_get_label, [key], tf.string, stateful=False)
            ground_truth = tf.image.decode_png(decoded_image, channels=1)
            ground_truth = tf.expand_dims(ground_truth, axis=0)
            ground_truth = tf.image.resize_nearest_neighbor(ground_truth, input_size)
            ground_truth = tf.squeeze(ground_truth, axis=0)

            return ground_truth

        imgFolder = os.path.join(self.config['images_in_dir'], mode)
        print("Loading images from " + str(imgFolder) + "...")

        if mode == "train":
            files = sorted([x.strip() for x in open(self.train_sequences, 'r')])
        elif mode == "val":
            files = sorted([x.strip() for x in open(self.val_sequences, 'r')])
        else:
            files = sorted([x.strip() for x in open(self.test_sequences, 'r')])

        print("------------ found {} files in {} dataset --------- ".format(len(files), mode))

        for file_name in files:
            city_name = file_name.split('_')[0]
            if not self.city_subset or len(self.city_subset) == 0 or city_name in self.city_subset:
                input_location = "{}/{}/{}".format(imgFolder, city_name, file_name)
                if not os.path.exists(input_location):
                    raise FileNotFoundError("File {} could not be loaded".format(input_location))

                examples.append(file_name)

        print("retained {} files".format(len(examples)))

        if shuffle:
            grouped_examples = [tuple(examples[x:x+time_steps]) for x in range(0, len(examples), time_steps) if
                                x + time_steps <= len(examples)]
            random.shuffle(grouped_examples)
            examples = []
            _ = [examples.extend(list(x)) for x in grouped_examples]

        input_dataset = tf.data.Dataset.from_tensor_slices(examples)
        input_dataset = input_dataset.map(_read_input_file, num_parallel_calls=16)
        if time_steps > 1:
            input_dataset = input_dataset.batch(time_steps)

        if include_groundtruth:
            if many_to_many:
                gt_dataset = tf.data.Dataset.from_tensor_slices(examples)
                gt_dataset = gt_dataset.map(_read_gt_file, num_parallel_calls=16)
                if time_steps > 1:
                    gt_dataset = gt_dataset.batch(time_steps)
                
                if include_filenames:
                    labels_dataset = tf.data.Dataset.from_tensor_slices(examples)
                    if time_steps > 1:
                        labels_dataset = labels_dataset.batch(time_steps)
                    input_dataset = tf.data.Dataset.zip((input_dataset, gt_dataset, labels_dataset))
                else:
                    input_dataset = tf.data.Dataset.zip((input_dataset, gt_dataset))

            else:
                gt_files = examples[time_steps-1::time_steps]
                gt_dataset = tf.data.Dataset.from_tensor_slices(gt_files)
                gt_dataset = gt_dataset.map(_read_gt_file, num_parallel_calls=16)

                if include_filenames:
                    labels_dataset = tf.data.Dataset.from_tensor_slices(gt_files)
                    input_dataset = tf.data.Dataset.zip((input_dataset, gt_dataset, labels_dataset))
                else:
                    input_dataset = tf.data.Dataset.zip((input_dataset, gt_dataset))

        input_dataset = input_dataset.batch(batch_size)
        return input_dataset







