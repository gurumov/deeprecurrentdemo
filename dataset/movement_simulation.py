import os
import numpy as np
from PIL import Image


def move_left(image, gt, number_of_frames):
    """
    a generator to simulate left lateral movement

    :param image:
    :param gt:
    :param number_of_frames: the number of frames to generate
    :return:
    """
    assert gt.shape[:2] == image.shape[:2], "the shape of image and gt provided doesn't match"

    flipped_image = np.fliplr(image)
    flipped_gt = np.fliplr(gt)
    lateral_movement = np.random.randint(25, 40)

    for i in range(number_of_frames):
        new_image = np.zeros_like(image)
        new_gt = np.zeros_like(gt)
        offset = (number_of_frames - 1 - i) * lateral_movement
        if offset == 0:
            new_image = image
            new_gt = gt
        else:
            new_image[:, :offset, :] = flipped_image[:, -1 * offset:, :]
            new_image[:, offset:, :] = image[:, : -1 * offset, :]

            new_gt[:, :offset] = flipped_gt[:, -1 * offset:]
            new_gt[:, offset:] = gt[:, : -1 * offset]

        assert new_image.shape == image.shape
        assert new_gt.shape == gt.shape

        yield new_image, new_gt


def right(image, gt, number_of_frames):
    """
    a generator to simulate right lateral movement

    :param image:
    :param gt:
    :param number_of_frames: the number of frames to generate
    :return:
    """
    assert gt.shape[:2] == image.shape[:2], "the shape of image and gt provided doesn't match"

    flipped_image = np.fliplr(image)
    flipped_gt = np.fliplr(gt)
    lateral_movement = np.random.randint(25, 40)

    for i in range(number_of_frames):
        new_image = np.zeros_like(image)
        new_gt = np.zeros_like(gt)
        offset = (number_of_frames - 1 - i) * lateral_movement
        if offset == 0:
            new_image = image
            new_gt = gt
        else:
            new_image[:, -1 * offset:, :] = flipped_image[:, :offset, :]
            new_image[:, : -1 * offset, :] = image[:, offset:, :]

            new_gt[:, -1 * offset:] = flipped_gt[:, :offset]
            new_gt[:, : -1 * offset] = gt[:, offset:]

        assert new_image.shape == image.shape
        assert new_gt.shape == gt.shape

        yield new_image, new_gt


def zoom(image, gt, number_of_frames):
    """
    a generator to simulate zooming in

    :param image:
    :param gt:
    :param number_of_frames: the number of frames to generate
    :return:
    """
    assert gt.shape[:2] == image.shape[:2], "the shape of image and gt provided doesn't match"

    lateral_movement = np.random.randint(10, 20)
    forward_movement = np.random.randint(5, 15)
    original_size = gt.shape

    gt_values = set(np.unique(gt))

    for i in range(number_of_frames):

        if i == 0:
            new_image = image
            new_gt = gt
        else:

            top_bottom_offset = i * forward_movement
            left_right_offset = i * lateral_movement
            new_image = np.zeros((image.shape[0] - 2*top_bottom_offset, image.shape[1] - 2*left_right_offset, image.shape[2]), dtype=np.uint8)
            new_gt = np.zeros((gt.shape[0] - 2*top_bottom_offset, gt.shape[1] - 2*left_right_offset), dtype=np.uint8)

            new_image[:, :, :] = image[top_bottom_offset: -top_bottom_offset, left_right_offset: -left_right_offset, :]
            new_gt[:, :] = gt[top_bottom_offset: -top_bottom_offset, left_right_offset: -left_right_offset]

        img = Image.fromarray(new_image).resize((original_size[1], original_size[0]), Image.BILINEAR)
        gt_img = Image.fromarray(new_gt).resize((original_size[1], original_size[0]), Image.NEAREST)

        res_gt_values = set(np.unique(np.asarray(gt_img)))

        if gt_values != res_gt_values:
            raise Exception("after zooming classes in the groundtruth have changed!")
        else:
            yield np.asarray(img), np.asarray(gt_img)








