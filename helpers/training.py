import tensorflow as tf

PS_OPS = ['Variable', 'VariableV2', 'AutoReloadVariable']


def average_gradients(tower_grads):
    """Calculate the average gradient for each shared variable across all towers.
    Note that this function provides a synchronization point across all towers.
    Args:
      tower_grads: List of lists of (gradient, variable) tuples. The outer list
        is over individual gradients. The inner list is over the gradient
        calculation for each tower.
    Returns:
       List of pairs of (gradient, variable) where the gradient has been averaged
       across all towers.
    """
    average_grads = []
    for grad_and_vars in zip(*tower_grads):
        # Note that each grad_and_vars looks like the following:
        #   ((grad0_gpu0, var0_gpu0), ... , (grad0_gpuN, var0_gpuN))
        grads = []
        for g, _ in grad_and_vars:
            # Add 0 dimension to the gradients to represent the tower.
            expanded_g = tf.expand_dims(g, 0)

            # Append on a 'tower' dimension which we will average over below.
            grads.append(expanded_g)

        # Average over the 'tower' dimension.
        grad = tf.concat(grads, axis=0)
        grad = tf.reduce_mean(grad, 0)

        # Keep in mind that the Variables are redundant because they are shared
        # across towers. So .. we will just return the first tower's pointer to
        # the Variable.
        v = grad_and_vars[0][1]
        grad_and_var = (grad, v)
        average_grads.append(grad_and_var)
    return average_grads


def assign_to_device(device, ps_device='/cpu:0'):
    """
    By default, all variables will be placed on '/gpu:0'
    We need a custom device function, to assign all variables to '/cpu:0'
    Note: If GPUs are peered, '/gpu:0' can be a faster option
    :param device: The device on which the operation should be allocated
    :param ps_device: a device vor Variables
    :return:
    """
    def _assign(op):
        node_def = op if isinstance(op, tf.NodeDef) else op.node_def
        if node_def.op in PS_OPS:
            return "/" + ps_device
        else:
            return device

    return _assign


def optimistic_restore(session, save_file, exclusion_list=[]):
    """
    A method to optimisticly load stored weights from a checkpoint file
    :param session: a refference to the session
    :param save_file: the location of the checkpoint file
    :param exclusion_list: if any variables should be omitted
    :return:
    """
    reader = tf.train.NewCheckpointReader(save_file)
    saved_shapes = reader.get_variable_to_shape_map()
    existing_var_names = sorted([(var.name, var.name.split(':')[0]) for var in tf.global_variables()
                                 if var.name.split(':')[0] in saved_shapes])

    filtered_varnames = [(var_name, saved_var_name) for var_name, saved_var_name in existing_var_names if not
    any(map(lambda x: x in var_name, exclusion_list))]

    nonexisting_var_names = [var for var in tf.global_variables() if
                             var.name.split(':')[0] not in saved_shapes]

    print('nonexisting vars ------------------')
    print(nonexisting_var_names)

    restore_vars = []
    name2var = dict(
        zip(map(lambda x: x.name.split(':')[0], tf.global_variables()), tf.global_variables()))
    with tf.variable_scope('', reuse=True):
        for var_name, saved_var_name in filtered_varnames:
            curr_var = name2var[saved_var_name]
            var_shape = curr_var.get_shape().as_list()
            if var_shape == saved_shapes[saved_var_name]:
                restore_vars.append(curr_var)
            else:
                print("couldn't restore {} with current shape {} but stored shape {}".format(
                    saved_var_name, var_shape, saved_shapes[saved_var_name]))
        print('restored_vars ------------------------------------')
        print(restore_vars)
        print(len(restore_vars))

        saver = tf.train.Saver(restore_vars, write_version=tf.train.SaverDef.V2)
        saver.restore(session, save_file)
        session.run(tf.variables_initializer(nonexisting_var_names))
