import numpy as np
import scipy as sp
import PIL
import os

LABEL_COLOR_LUT = {"cityscapes": np.array([(128, 64, 128), (244, 35, 232), (70, 70, 70), (102, 102, 156),
                                           (190, 153, 153), (153, 153, 153), (250, 170, 30), (220, 220, 0),
                                           (107, 142, 35), (152, 251, 152), (70, 130, 180), (220, 20, 60),
                                           (255, 0, 0), (0, 0, 142), (0, 0, 70), (0, 60, 100),
                                           (0, 80, 100), (0, 0, 230), (119, 11, 32)]),
                   "lanes": np.array([(0, 196, 121), (255, 210, 37), (240, 127, 0), (246, 22, 70),
                                      (255, 255, 255), (224, 74, 209), (230, 150, 140), (70, 70, 70), (102, 102, 156),
                                      (190, 153, 153), (180, 165, 180), (150, 100, 100), (220, 220, 0), (107, 142, 35),
                                      (220, 20, 60), (255, 0, 0), (0, 0, 70), (0, 60, 100), (0, 0, 90),
                                      (140, 0, 160), (255, 0, 200), (255, 140, 230), (221, 147, 255), (0, 0, 0)])

                   }

ALPHA = 0.4


def _ensure_path(path):
    path = os.path.dirname(path)
    if not os.path.exists(path):
        os.makedirs(path)

def write_debug_label_img(labels, image, metadata, path, key="cityscapes"):
    """
    A method to plot the resulting labels on top of the input image
    :param labels: the predicted segmentation mask
    :param image:
    :param metadata: a dict with metadata about the example
    :param path: path where the produced examples should be saved to
    :param key: which visualization mode to be used
    :return:
    """
    image_path = os.path.join(path, 'label_debug', metadata['key'].decode('utf-8') + '.jpg')
    _ensure_path(image_path)
    label = np.where(labels > 22, 23, labels)
    image = sp.misc.toimage((1.0 - ALPHA) * LABEL_COLOR_LUT[key][label[0]] +
                            ALPHA * image[0], cmin=0, cmax=255)
    image = image.resize((metadata['w'], metadata['h']), PIL.Image.NEAREST)
    image.save(image_path)