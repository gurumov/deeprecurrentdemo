import yaml
import os
import numpy as np
import tensorflow as tf

from dataset.cityscapes_multiple import CityscapesDatasetGenerator
from helpers.training import  optimistic_restore
from helpers.visualization import write_debug_label_img

from models.recurrent import GruNet

def inference(config):
    model_dir = config["model_dir"]
    num_classes = config["num_classes"]
    visible = config['visible']
    output_dir = config['output_dir']
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = visible

    with tf.Graph().as_default():

        config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        config_proto.gpu_options.allow_growth = True

        with tf.Session(config=config_proto) as session:
            with tf.device('/cpu:0'):

                cityscapes_validation = CityscapesDatasetGenerator(config, 'val')
                dataset = cityscapes_validation.get_dataset('val',
                                                            include_groundtruth=False,
                                                            include_filenames=False,
                                                            many_to_many=False,
                                                            time_steps=3,
                                                            input_size=(256, 512),
                                                            batch_size=1,
                                                            shuffle=False)

                training_iterator = dataset.make_initializable_iterator()
                session.run(training_iterator.initializer)

            with tf.variable_scope('vars'):

                net = GruNet("net", config)
                images = training_iterator.get_next()
                results = net(images)


            last_checkpoint = tf.train.get_checkpoint_state(model_dir)
            if last_checkpoint and last_checkpoint.model_checkpoint_path:
                print('Found checkpoint, loading')
                optimistic_restore(session, last_checkpoint.model_checkpoint_path)
            else:
                print("couldn't find a checkpoint!")
                return

            while True:
                res, image = session.run([results, images])
                max_res = np.argmax(res, axis=-1)


                write_debug_label_img(max_res, image, {"w": 512, "h": 256}, output_dir, key="lanes")

            session.close()


if __name__ == "__main__":
    with open("./config.yaml", 'r') as stream:
        config = yaml.load(stream)

    inference(config)
