import tensorflow as tf

def custom_categorial_loss(y_true, y_pred, num_classes=23, class_weights=None):
    """
    A custom implementation of categorical Loss, avoiding pixles with 'Ignore' label
    :param y_true: ground truth labels
    :param y_pred: network predictions
    :param num_classes: number of classes
    :param class_weights: optional weight array for the different classes
    :return: The loss op
    """
    y_pred = tf.reshape(y_pred, [-1, num_classes])
    y_true = tf.cast(y_true, tf.int32)
    y_true = tf.reshape(y_true, [-1])

    size_assert = tf.assert_equal(tf.shape(y_pred)[0], tf.shape(y_true)[0], message="different shapes of prediction and\
                                                                                    gt encountered")
    with tf.control_dependencies([size_assert]):
        y_true = tf.identity(y_true, "y_true")

    num_ignore = tf.reduce_sum(tf.cast(tf.math.greater(y_true, num_classes-1), tf.int32))
    gradient_mask = tf.cast(tf.math.less(y_true, num_classes), tf.int32)

    epsilon = tf.constant(0.00001, dtype=tf.float32)

    if class_weights is not None and len(class_weights) == num_classes:
        gradient_mask = tf.where(tf.equal(gradient_mask, 1),
                                 tf.gather(class_weights, y_true),
                                 tf.zeros_like(y_true, dtype=tf.float32))

    cross_entropy = tf.losses.softmax_cross_entropy(onehot_labels=tf.one_hot(y_true, num_classes),
                                                    logits=y_pred,
                                                    weights=gradient_mask,
                                                    reduction=tf.losses.Reduction.NONE)

    loss = tf.div(tf.reduce_sum(cross_entropy), tf.cast(tf.size(y_true) - num_ignore, tf.float32)  + epsilon)
    return loss

