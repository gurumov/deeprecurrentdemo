import tensorflow as tf


def custom_accuracy(y_true, y_pred, num_classes=23):
    """
    This function takes the ground truth labels and the network prediction and returns operations, for the comupation
    of the image accuracy, the mean accuracy, precision, recall and f1 score as well as the same per class

    :param y_true: ground truth labels
    :param y_pred: network predictions
    :param num_classes: number of classes
    :return: accuracy, mean_accuracy, mean_precision, mean_recall, mean_f1, acc_per_class, \
           recall_per_class, precision_per_class, f1_per_class
    """
    y_pred = tf.reshape(y_pred, [-1, num_classes])
    y_true = tf.cast(y_true, tf.int32)
    y_true = tf.reshape(y_true, [-1])
    pred = tf.cast(tf.argmax(tf.nn.softmax(y_pred, axis=-1), axis=-1), tf.int32)

    label_mask = tf.math.less(y_true, num_classes)
    masked_gt = tf.boolean_mask(y_true, label_mask)
    masked_prediction = tf.boolean_mask(pred, label_mask)

    gt_one_hot = tf.cast(tf.one_hot(masked_gt, depth=num_classes), tf.bool)
    instances_per_class = tf.reduce_sum(tf.cast(gt_one_hot, dtype=tf.int32), axis=0)
    masked_one_hot = tf.cast(tf.one_hot(masked_prediction, depth=num_classes), tf.bool)

    intersection = tf.logical_and(gt_one_hot, masked_one_hot)
    union = tf.logical_or(gt_one_hot, masked_one_hot)
    not_union = tf.logical_not(union)
    different = tf.logical_xor(gt_one_hot, masked_one_hot)

    fp = tf.logical_and(masked_one_hot, different)
    fn = tf.logical_and(gt_one_hot, different)

    tp_per_class = tf.cast(tf.reduce_sum(tf.cast(intersection, tf.int32), axis=0), dtype=tf.float32)
    tn_per_class = tf.cast(tf.reduce_sum(tf.cast(not_union, tf.int32), axis=0), dtype=tf.float32)
    fp_per_class = tf.cast(tf.reduce_sum(tf.cast(fp, tf.int32), axis=0), dtype=tf.float32)
    fn_per_class = tf.cast(tf.reduce_sum(tf.cast(fn, tf.int32), axis=0), dtype=tf.float32)

    all_per_class = tp_per_class + tn_per_class + fp_per_class + fn_per_class

    with tf.control_dependencies([tf.assert_equal(tf.size(masked_gt) * num_classes,
                                                  tf.cast(tf.reduce_sum(all_per_class), dtype=tf.int32))]):
        correct_prediction = tf.equal(masked_prediction, masked_gt)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        accuracy = tf.cond(tf.is_nan(accuracy), lambda: 0.0, lambda: accuracy)

        precision_per_class = tp_per_class / (tp_per_class + fp_per_class + 1e-6)
        precision_per_class = tf.where(tf.equal(instances_per_class, 0), tf.ones_like(precision_per_class),
                                       precision_per_class)

        recall_per_class = tp_per_class / (tp_per_class + fn_per_class + 1e-6)
        recall_per_class = tf.where(tf.equal(instances_per_class, 0), tf.ones_like(precision_per_class),
                                    recall_per_class)

        acc_per_class = (tp_per_class + tn_per_class) / all_per_class

        f1_per_class = (2 * precision_per_class * recall_per_class) / (precision_per_class + recall_per_class + 1e-6)

        mean_accuracy = tf.reduce_mean(acc_per_class)
        mean_precision = tf.reduce_mean(precision_per_class)
        mean_recall = tf.reduce_mean(recall_per_class)
        mean_f1 = tf.reduce_mean(f1_per_class)

        return accuracy, mean_accuracy, mean_precision, mean_recall, mean_f1, acc_per_class, \
               recall_per_class, precision_per_class, f1_per_class
