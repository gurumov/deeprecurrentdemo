import tensorflow as tf


def custom_iou(y_true, y_pred, num_classes=23):
    """
    A function to compute the IoU score given ground truth and network predictions
    :param y_true: ground truth labels
    :param y_pred: network predictions
    :param num_classes: number of classes
    :return: mean_iou and iou_per_class
    """
    y_pred = tf.reshape(y_pred, [-1, num_classes])
    y_true = tf.cast(y_true, tf.int32)
    y_true = tf.reshape(y_true, [-1])

    pred = tf.cast(tf.argmax(tf.nn.softmax(y_pred, axis=-1), axis=-1), tf.int32)

    label_mask = tf.math.less(y_true, num_classes)

    masked_gt = tf.boolean_mask(y_true, label_mask)
    gt_one_hot = tf.cast(tf.one_hot(masked_gt, depth=num_classes), tf.bool)

    masked_prediction = tf.boolean_mask(pred, label_mask)
    masked_one_hot = tf.cast(tf.one_hot(masked_prediction, depth=num_classes), tf.bool)

    intersection = tf.logical_and(gt_one_hot, masked_one_hot)
    union = tf.logical_or(gt_one_hot, masked_one_hot)

    intersection_count = tf.reduce_sum(tf.cast(intersection, tf.int32), axis=0)
    union_count = tf.reduce_sum(tf.cast(union, tf.int32), axis=0)
    iou_per_class = intersection_count / union_count

    iou_per_class = tf.where(tf.is_nan(iou_per_class), tf.ones_like(iou_per_class), iou_per_class)
    mean_iou = tf.reduce_mean(iou_per_class)

    return mean_iou, iou_per_class,
