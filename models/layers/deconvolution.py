import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Conv2DTranspose
from tensorflowkeras.initializers import glorot_normal
from tensorflow.keras.regularizers import l2

REGULARIZER_WEIGHT=1e-5

class DeconvBlock_simple(tf.keras.Model):
    def __init__(self, name, middle_channels, out_channels):
        super().__init__(name=name)
        self.out_channels = out_channels
        self.conv = Conv2D(middle_channels, (3, 3),
                                        padding='same',
                                        strides=(1, 1),
                                        activation="relu",
                                        kernel_initializer=glorot_normal(),
                                        kernel_regularizer=l2(REGULARIZER_WEIGHT),
                                        name='deconv')

        self.transposed = Conv2DTranspose(out_channels, (4, 4), strides=(2, 2),
                                          padding='same',
                                          activation=tf.nn.relu,
                                          kernel_initializer=glorot_normal(),
                                          kernel_regularizer=l2(REGULARIZER_WEIGHT),
                                          name='transposed_conv')

    def call(self, x):
        x = self.conv(x)
        x = self.transposed(x)
        return x

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1] * 2, input_shape[2] * 2, self.out_channels)
