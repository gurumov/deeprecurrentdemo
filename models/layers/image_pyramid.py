import tensorflow as tf
from tensorflowkeras.initializers import glorot_normal
from tensorflow.keras.regularizers import l2

REGULARIZER_WEIGHT = 1e-5


class TfPoolingPyramid(tf.keras.layers.Layer):

    def __init__(self, name, atrous_rates, downscale=8, use_layer_norm=False, **kwargs):
        super().__init__(name=name, **kwargs)
        self.atrous_rates = atrous_rates
        self.use_layer_norm = use_layer_norm
        self.downscale = downscale

    def build(self, input_shapes):
        self.final_shape = (int(input_shapes[-3] / self.downscale), int(input_shapes[-2] / self.downscale))
        self.avgPool = tf.keras.layers.AveragePooling2D(self.final_shape)

        self.conv1 = tf.keras.layers.Conv2D(256, (1, 1), padding='same',
                                            use_bias=False, name='image_pooling', activation='elu')

        self.conv2 = tf.keras.layers.Conv2D(256, (1, 1), padding='same',
                                            use_bias=False, name='aspp0', activation='elu')

        self.conv3 = tf.keras.layers.Conv2D(256, (1, 1), padding='same',
                                            use_bias=False, name='concatenate_aggregation', activation='elu')

        self.separable1 = tf.keras.layers.SeparableConv2D(256,
                                                          (3, 3),
                                                          padding='same',
                                                          strides=(1, 1),
                                                          activation='elu',
                                                          dilation_rate=self.atrous_rates[0],
                                                          depthwise_initializer=glorot_normal(),
                                                          pointwise_initializer=glorot_normal(),
                                                          depthwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          pointwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          name='aspp1')

        self.separable2 = tf.keras.layers.SeparableConv2D(256,
                                                          (3, 3),
                                                          padding='same',
                                                          strides=(1, 1),
                                                          activation='elu',
                                                          dilation_rate=self.atrous_rates[1],
                                                          depthwise_initializer=glorot_normal(),
                                                          pointwise_initializer=glorot_normal(),
                                                          depthwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          pointwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          name='aspp2')

        self.separable3 = tf.keras.layers.SeparableConv2D(256,
                                                          (3, 3),
                                                          padding='same',
                                                          strides=(1, 1),
                                                          activation='elu',
                                                          dilation_rate=self.atrous_rates[2],
                                                          depthwise_initializer=glorot_normal(),
                                                          pointwise_initializer=glorot_normal(),
                                                          depthwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          pointwise_regularizer=l2(REGULARIZER_WEIGHT),
                                                          name='aspp3')

        self.concatenate = tf.keras.layers.Concatenate()

    def call(self, x):
        b4 = self.avgPool(x)
        b4 = self.conv1(b4)
        b4 = tf.image.resize_bilinear(b4, self.final_shape, align_corners=True)

        b0 = self.conv2(x)

        b1 = self.separable1(x)
        b2 = self.separable2(x)
        b3 = self.separable3(x)

        x = self.concatenate([b4, b0, b1, b2, b3])

        x = self.conv3(x)

        return x

    def compute_output_shape(self, input_shape):
        return (input_shape[0], input_shape[1], input_shape[2], 256)
