import tensorflow as tf
from tensorflow.keras.layers import Layer
from tensorflow.keras.initializers import Ones, Zeros


class LayerNorm2D_time(Layer):

    def __init__(self, name, eps=1e-6, **kwargs):
        self.eps = eps
        super(LayerNorm2D_time, self).__init__(name=name, **kwargs)

    def build(self, input_shape):
        self.gamma = self.add_weight(name="gamma", shape=input_shape[2:], initializer=Ones(),
                                     trainable=True)

        self.beta = self.add_weight(name="beta", shape=[input_shape[-1]], initializer=Zeros(),
                                    trainable=True)
        super().build(input_shape)

    def call(self, input_tensor):
        mean = tf.reduce_mean(input_tensor, axis=-1, keepdims=True)
        std = tf.std(input_tensor, axis=-1, keepdims=True)
        normed_input = (input_tensor - mean) / (std + self.eps)
        return self.gamma * normed_input + self.beta

    def compute_output_shape(self, input_shape):
        return input_shape
