import tensorflow as tf


class ResidualModule(tf.keras.Model):
    def __init__(self, name, num_output_channels=None, stride=1, dilation_rate=1, use_layer_norm=False, dropout1=0.0,
                 dropout2=0.0):
        super().__init__(name=name)
        self.num_output_channels = num_output_channels
        self.stride = stride
        self.dilation_rate = dilation_rate
        self.use_layer_norm = use_layer_norm
        self.dropout_rate1 = dropout1
        self.dropout_rate2 = dropout2

    def build(self, input_shapes):

        """
            Not included in demo. Please purchase the full product ;)

        """

    def call(self, x):

        return x

    def compute_output_shape(self, input_shape):
        print(input_shape)
        if self.num_output_channels is not None and self.num_output_channels != input_shape[-1]:
            return (input_shape[0], input_shape[1], input_shape[2], self.num_output_channels)
        else:
            return input_shape
