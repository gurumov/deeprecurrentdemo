import tensorflow as tf


class ResidualConvGru(tf.keras.Model):
    def __init__(self, name, filters, kernel_size, padding='same', return_sequences=False, use_layer_norm=True):
        super().__init__(name=name)

        self.filters = filters
        self.kernel_size = kernel_size
        self.padding = padding
        self.return_sequences = return_sequences
        self.use_layer_norm = use_layer_norm
        self.name = name

    def build(self, input_shape):

        """
         Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
         dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
         ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
         nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
         anim id est laborum.

        """

    def call(self, x):
        residual = x
        result = self.gru(x)
        result = tf.keras.activations.relu(result)

        if self.residual_conv is not None:
            residual = self.residual_conv(residual)
            if self.return_sequences:
                result += residual
                result = self.bn(result)
                return result
            else:
                last_residual = residual[:, -1]
                result += last_residual
                result = self.bn(result)
                return result
        else:
            if self.return_sequences:
                result += residual
                result = self.bn(result)
                return result
            else:
                last_residual = residual[:, -1]
                result += last_residual
                result = self.bn(result)
                return result

    def compute_output_shape(self, input_shape):
        if self.return_sequences:
            if input_shape[-1] == self.filters:
                return input_shape
            else:
                return (input_shape[0], input_shape[1], input_shape[2], input_shape[3], int(self.filters))
        else:
            return (input_shape[0], input_shape[2], input_shape[3], int(self.filters))
