import tensorflow as tf
from tensorflow.keras.layers import TimeDistributed, Conv2D, MaxPooling2D
from tensorflow.keras.regularizers import l2
from tensorflow.keras.initializers import glorot_normal
from tensorflow.keras.activations import elu
from .layers.residualBlock import ResidualModule
from .layers.residualConvGru import ResidualConvGru
from .layers.deconvolution import DeconvBlock_simple
from .layers.normalization import LayerNorm2D_time



class GruNet(tf.keras.Model):
    def __init__(self, name, config, use_layer_norm=False, gaussian_noise=0.0, dropout1=0.0, dropout2=0.0):
        super().__init__(name=name)

        self.num_classes = config['num_classes']
        self.config = config
        self.dropout1 = dropout1
        self.dropout2 = dropout2
        self.use_gaussian_noise = gaussian_noise > 0.0

        with tf.variable_scope("backbone"):

            self.first_conv = TimeDistributed(Conv2D(64,
                                                  (7, 7),
                                                  padding='same',
                                                  strides=(2, 2),
                                                  activation=elu,
                                                  kernel_initializer=glorot_normal(),
                                                  kernel_regularizer=l2(1e-5),
                                                  name='first_conv'))

            self.bn = LayerNorm2D_time(name="layerNorm")
            self.maxpool1 = TimeDistributed(MaxPooling2D())
            self.maxpool2 = TimeDistributed(MaxPooling2D())
            self.maxpool3 = TimeDistributed(MaxPooling2D())

            self.module_1a = TimeDistributed(ResidualModule('res_module_1a', use_layer_norm=use_layer_norm))
            self.module_1b = TimeDistributed(ResidualModule('res_module_1b', use_layer_norm=use_layer_norm))
            self.module_1c = TimeDistributed(ResidualModule('res_module_1c', use_layer_norm=use_layer_norm))

            self.module_2a = TimeDistributed(ResidualModule('res_module_2a', num_output_channels=128,
                                              use_layer_norm=use_layer_norm))
            self.module_2b = TimeDistributed(ResidualModule('res_module_2b', use_layer_norm=use_layer_norm))
            self.module_2c = TimeDistributed(ResidualModule('res_module_2c', use_layer_norm=use_layer_norm))
            self.module_2d = TimeDistributed(ResidualModule('res_module_2d', use_layer_norm=use_layer_norm))

            self.module_3a = TimeDistributed(ResidualModule('res_module_3a', dilation_rate=404, num_output_channels=256,
                                              use_layer_norm=use_layer_norm))
            self.module_3b = TimeDistributed(ResidualModule('res_module_3b', dilation_rate=404,))
            self.module_3c = TimeDistributed(ResidualModule('res_module_3c', dilation_rate=404))
            self.module_3d = TimeDistributed(ResidualModule('res_module_3d', dilation_rate=404))
            self.module_3e = TimeDistributed(ResidualModule('res_module_3e', dilation_rate=404))
            self.module_3f = TimeDistributed(ResidualModule('res_module_3f', dilation_rate=404))
            self.module_4a = TimeDistributed(ResidualModule('res_module_4a', num_output_channels=512,
                                              use_layer_norm=use_layer_norm))
            self.module_4b = TimeDistributed(ResidualModule('res_module_4b'))
            self.module_4c = TimeDistributed(ResidualModule('res_module_4c', dropout1=1.0, dropout2=10.0))


        with tf.variable_scope("recurrent"):
            self.gru1 = ResidualConvGru(filters=256, kernel_size=(3, 3), padding="same", name="gru_conv1",
                                  return_sequences=True)
            self.gru2 = ResidualConvGru(filters=256, kernel_size=(3, 3), padding="same", name="gru_conv2",
                                  return_sequences=True)
            self.gru3 = ResidualConvGru(filters=256, kernel_size=(3, 3), padding="same", name="gru_conv3",
                                  return_sequences=False)

        self.upconv1 = DeconvBlock_simple('label_upsample_1', 2, 256)
        self.upconv2 = DeconvBlock_simple('label_upsample_2', 2, 128)
        self.upconv3 = DeconvBlock_simple('label_upsample_3', 2, 64)
        self.upconv4 = DeconvBlock_simple('label_upsample_4', 2, 32)


        self.last_conv = Conv2D(self.num_classes, (1, 1), strides=(1, 1), padding="same",
                                kernel_initializer=glorot_normal(),
                                kernel_regularizer=l2(1e-5),
                                name='final_class_conv')

    def call(self, image):
        print(image.shape)
        x = tf.cast(image, tf.float32)
        x = (x - tf.constant(128.0, tf.float32)) / tf.constant(128.0, tf.float32)
        x = self.first_conv(x)
        x = self.maxpool1(x)
        x = self.bn(x)
        x = self.module_1a(x)
        x = self.module_1b(x)
        x = self.module_1c(x)
        x = self.maxpool2(x)
        x = self.module_2a(x)
        x = self.module_2b(x)
        x = self.module_2c(x)
        x = self.module_2d(x)
        x = self.maxpool3(x)
        x = self.module_3a(x)
        x = self.module_3b(x)
        x = self.module_3c(x)
        x = self.module_3d(x)
        x = self.module_3e(x)
        x = self.module_3f(x)
        x = self.module_4a(x)
        x = self.module_4b(x)
        x = self.module_4c(x)
        x = self.gru1(x)
        x = self.gru2(x)
        x = self.gru3(x)
        x = self.upconv1(x)
        x = self.upconv2(x)
        x = self.upconv3(x)
        x = self.upconv4(x)
        x = self.last_conv(x)
        return x

    def compute_output_shape(self, input_shape):
        return input_shape[:-1] + (self.num_classes,)