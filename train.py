#!/usr/bin/env python3
import yaml
import os
import numpy as np
import tensorflow as tf

from dataset.cityscapes_multiple import CityscapesDatasetGenerator
from helpers.training import assign_to_device, average_gradients, optimistic_restore
from losses.categorical import custom_categorial_loss
from metrics.iou import custom_iou
from metrics.accuracy import custom_accuracy
from evaluation import run_eval_loop
from models.recurrent import GruNet

def create_datasets(config, input_size, time_steps, batch_size, include_groundtruth, include_labels, many_to_many,
                    shuffle):
    """
    :param config: a reference to the general config file
    :param input_size: the size to which the data should be resized
    :param time_steps: number of time steps per example
    :param batch_size: number of examples in batch
    :param include_groundtruth: if the ground truth labels should be included
    :param include_filenames: If the file names of the examples should be included
    :param many_to_many: If ground truth should be returned for all examples or only the last one
    :param shuffle: if examples should be shuffled
    :return: dataset iterator op,  initializing tensors for training and validation dataset
    """

    cityscapes_train = CityscapesDatasetGenerator(config, 'train')
    cityscapes_val = CityscapesDatasetGenerator(config, 'val')

    training_dataset = cityscapes_train.get_dataset('train',
                                                    include_groundtruth=include_groundtruth,
                                                    include_labels=include_labels,
                                                    many_to_many=many_to_many,
                                                    batch_size=batch_size,
                                                    time_steps=time_steps,
                                                    input_size=input_size,
                                                    shuffle=shuffle)

    validation_dataset = cityscapes_val.get_dataset('val',
                                                    include_groundtruth=include_groundtruth,
                                                    include_labels=include_labels,
                                                    many_to_many=many_to_many,
                                                    batch_size=batch_size,
                                                    time_steps=time_steps,
                                                    input_size=input_size,
                                                    shuffle=shuffle)

    training_dataset = training_dataset.repeat()
    training_dataset = training_dataset.prefetch(batch_size)

    validation_dataset = validation_dataset.repeat()
    validation_dataset = validation_dataset.prefetch(batch_size)

    dataset_iterator = tf.data.Iterator.from_structure(training_dataset.output_types,
                                                       training_dataset.output_shapes)

    train_init_op = iter.make_initializer(training_dataset)
    val_init_op = iter.make_initializer(validation_dataset)

    return dataset_iterator, train_init_op, val_init_op


def gather_summaries(loss_op, iou_op, iou_per_class_op, accuracy_op, mean_precision_op,
                     mean_recall_op, mean_f1_op, acc_per_class_op, recall_per_class_op, precision_per_class_op,
                     f1_per_class_op, num_classes):
    """
    A method to gather the summaries for tensorboard
    :param loss_op:
    :param iou_op:
    :param iou_per_class_op:
    :param accuracy_op:
    :param mean_precision_op:
    :param mean_recall_op:
    :param mean_f1_op:
    :param acc_per_class_oprecall_per_class_op:
    :param precision_per_class_op:
    :param f1_per_class_op:
    :param num_classes:
    :return:
    """

    tf.summary.scalar("loss", loss_op)
    tf.summary.scalar("iou", iou_op)
    tf.summary.scalar("accuracy", accuracy_op)
    tf.summary.scalar("precision", mean_precision_op)
    tf.summary.scalar("recall", mean_recall_op)
    tf.summary.scalar("f1", mean_f1_op)

    for cls in range(num_classes):
        tf.summary.scalar("iou_{}".format(cls), tf.gather(iou_per_class_op, cls))
        tf.summary.scalar("acc_{}".format(cls), tf.gather(acc_per_class_op, cls))
        tf.summary.scalar("recall_{}".format(cls), tf.gather(recall_per_class_op, cls))
        tf.summary.scalar("precision_{}".format(cls),
                          tf.gather(precision_per_class_op, cls))
        tf.summary.scalar("f1_{}".format(cls), tf.gather(f1_per_class_op, cls))

    return tf.summary.merge_all()


def train(config):
    model_dir = config["model_dir"]
    num_classes = config["num_classes"]
    num_gpus = config['num_gpus']
    epochs = config['epochs']
    batch_size = config['batch_size']
    steps_per_epoch = config['steps_per_epoch']
    visible = config['visible']
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = visible
    max_steps = epochs * steps_per_epoch

    with tf.Graph().as_default():

        config_proto = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        config_proto.gpu_options.allow_growth = True

        with tf.Session(config=config_proto) as session:

            with tf.device('/cpu:0'):

                dataset_iterator, train_init_op, val_init_op = create_datasets(config,
                                                                               input_size=(256, 512),
                                                                               time_steps=3,
                                                                               batch_size=batch_size,
                                                                               include_groundtruth=True,
                                                                               include_labels=False,
                                                                               many_to_many=False,
                                                                               shuffle=True)

                global_step = tf.Variable(0, name="global_step_name", trainable=False)

                tower_grads = []

                with tf.variable_scope('vars', reuse=tf.AUTO_REUSE):

                    lrate = 0.01 * tf.math.pow(0.5, tf.math.minimum(
                        tf.cast(tf.math.floor((1 + global_step) / 1000), dtype=tf.float32), 7.0))

                    optimizer = tf.train.AdamOptimizer(lrate, beta1=0.7)

                    net = GruNet("net", config)

                    loss_function = custom_categorial_loss

                    for i in range(num_gpus):
                        with tf.device(assign_to_device('/gpu:%d' % i, ps_device='/cpu:0')):

                            image, labels = dataset_iterator.get_next()
                            results = net(image)

                            loss_op = loss_function(y_true=labels, y_pred=results, num_classes=num_classes)

                            if i == 0:
                                iou_op, iou_per_class_op = custom_iou(y_true=labels,
                                                                      y_pred=results,
                                                                      num_classes=num_classes)

                                accuracy_op, mean_accuracy_op, mean_precision_op, mean_recall_op, mean_f1_op, \
                                acc_per_class_op, recall_per_class_op, precision_per_class_op, f1_per_class_op, = \
                                    custom_accuracy(y_true=labels, y_pred=results, num_classes=num_classes)

                                update_ops = net.updates
                                loss_op = loss_op + tf.add_n(net.losses)

                                merged_summaries = gather_summaries(loss_op, iou_op, iou_per_class_op, accuracy_op,
                                                                    mean_precision_op, mean_recall_op, mean_f1_op,
                                                                    acc_per_class_op, recall_per_class_op,
                                                                    precision_per_class_op, f1_per_class_op,
                                                                    num_classes)
                            # Calculate the gradients for the batch of data
                            grads_and_vars = optimizer.compute_gradients(loss_op, net.trainable_weights,
                                                                         colocate_gradients_with_ops=True)
                            # Keep track of the gradients across all towers.
                            tower_grads.append(grads_and_vars)
                            # Reuse variables for the next tower.
                            tf.get_variable_scope().reuse_variables()

                # We must calculate the mean of each gradient. This is the synchronization point across all towers.
                grads = average_gradients(tower_grads)
                # Apply the gradients to adjust the shared variables.
                apply_gradient_op = optimizer.apply_gradients(grads, global_step=global_step)
                # Group all update OPs to into a single OP.
                update_ops = tf.group(*update_ops)
                # # Group all updates to into a single train op.
                train_op = tf.group(apply_gradient_op, update_ops)

                train_writer = tf.summary.FileWriter('./logs/train', session.graph)
                val_writer = tf.summary.FileWriter('./logs/val')

                saver = tf.train.Saver(tf.all_variables())

                last_checkpoint = tf.train.get_checkpoint_state(config['checkpoint_dir'])
                if last_checkpoint and last_checkpoint.model_checkpoint_path:
                    print('Found checkpoint, resuming training')
                    optimistic_restore(session, last_checkpoint.model_checkpoint_path)
                else:
                    print("initializing  global variables anew")
                    session.run(tf.global_variables_initializer())

                session.run(tf.local_variables_initializer())

                step = session.run(global_step)
                session.run(train_init_op)

                print("start")
                while step < max_steps:

                    if step > 0 and step % steps_per_epoch == 0:
                        print("---------------validation-----------------")
                        session.run(val_init_op)

                        validation_summary = run_eval_loop(session, dataset_iterator)
                        summary = session.run(validation_summary)
                        val_writer.add_summary(summary, step)

                        session.run(train_init_op)

                    else:
                        print("----------------training--------------------")
                        _, loss, summary = session.run([train_op, loss_op, merged_summaries])
                        train_writer.add_summary(summary, step)

                        assert not np.isnan(loss), 'Model diverged with loss = NaN'

                        # Save the model checkpoint periodically.
                        if step > 0 and (step % steps_per_epoch == 0 or (step + 1) == config['max_steps']):
                            checkpoint_path = os.path.join(model_dir, 'model.ckpt')
                            saver.save(session, checkpoint_path, global_step=step)

                        step = session.run(global_step)

                train_writer.close()
                val_writer.close()
                session.close()


if __name__ == "__main__":
    with open('./config.yaml', 'r') as stream:
        config = yaml.load(stream)
    train(config)
